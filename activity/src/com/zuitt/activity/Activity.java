package com.zuitt.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Activity {
    public static void main(String[] args) {

    //No.1
        int[] primeArray = new int[5];
        primeArray[0] = 2;
        primeArray[1] = 3;
        primeArray[2] = 5;
        primeArray[3] = 7;
        primeArray[4] = 11;
        System.out.println("The first prime number is: " + primeArray[0]);

    //No.2
        ArrayList<String> friends = new ArrayList<String>(Arrays.asList("Luffy", "Zorro", "Sanji", "Chooper"));
        System.out.println("My friends are: " + friends);

    //No.3
        HashMap<String, Integer> inventory = new HashMap<String, Integer>();
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);
        System.out.println("Our current inventory consists of: " + inventory);

    }
}
